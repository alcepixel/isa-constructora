<?php get_header();
/* Site Showcase */
imic_page_banner($pageID = get_the_ID());
/* End Site Showcase */
/* Login/Register Page Design Layout 
============================================*/
$pageLayout = get_post_meta(get_the_ID(),'imic_register_layout',true);
$contentClass = 4;
if ($pageLayout != 1) { $contentClass = 6; }?>
<!-- Start Content -->
<div class="main" role="main">
  <div id="content" class="content full">
    <div class="container">
      <div class="page">
        <div class="row">
          <?php echo '<div class="col-md-6 col-sm-6">';
            /* Page Content =====================*/
            while (have_posts()):the_post();
              the_content();
            endwhile;
          echo '</div>';
          /* Manage Login Form ========================*/
          if ($pageLayout == 1 || $pageLayout == 2) { ?>





            <div class="col-md-6 col-sm-6 login-form">
              <h3><?php _e('Inversionista','framework'); ?></h3>
        
             <?php
              echo do_shortcode( '[wp-members page="login"]' );

               if ( is_user_logged_in() ) {

             ?>
             <?php
                  $current_user = wp_get_current_user();
                  $us = get_current_user_id();

                  $args = array(
                        'post_type' => 'informe',
                        'meta_query' => array(
                              array(
                                    'key' => 'usuario_inver',
                                    'value' => $us
                                  ) 
                                  )
                    );

                    $latest = new WP_Query($args); 

                     if($latest->have_posts()) {   

                    while( $latest->have_posts() ) : $latest->the_post();
                

                    echo "<h1>".get_the_title()."</h1>";
                  

                      the_content();

                 
                   


                    endwhile; ?>


                  
             


            <?php } 

              else{
                echo "<h3>No existen documentos asociados a su cuenta.</h3>";
              }

          }?>



          <?php } ?>

        </div>
      </div>
    </div>
  </div>
</div>
<?php  get_footer(); ?>