<?php /* Template Name: Contact */ get_header();
  /* Site Showcase */
    imic_page_banner($pageID = get_the_ID());
  /* End Site Showcase */
?>
  <!-- Start Content -->
  <div class="main" role="main">
    <div id="content" class="content full">
      <div class="container">
        <div class="page">
          <div class="row">
            <div class="col-md-6 col-sm-6">
              <h3><?php _e('Formulario de Contacto', 'framework'); ?></h3>
              <?php echo do_shortcode( '[contact-form-7 id="901" title="Contacto Constructora"]' ); ?>
            </div>
            <div class="col-md-6 col-sm-6">
              <h3><?php _e('Nuestra Información', 'framework'); ?></h3>
              <div class="padding-as25 lgray-bg">
              	<?php /* Contact Page Details */
        					$address = '';
        					$address_for_map = get_post_meta(get_the_ID(), 'imic_our_location_address', true);
        					if(have_posts()):while(have_posts()):the_post();
          					the_content();
        					endwhile; endif;
                  $property_longitude_and_latitude=getLongitudeLatitudeByAddress($address_for_map);
                  echo '<div id="contact'.get_the_ID().'" class ="property_container" style="display:none;"><span class ="property_address">'.$address_for_map.'</span><span class ="latitude">'.$property_longitude_and_latitude[0].'</span><span class ="longitude">'.$property_longitude_and_latitude[1].'</span><span class ="property_image_url">'.IMIC_THEME_PATH.'/images/map-marker.png</span></div>';
                ?>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
<?php get_footer(); ?>