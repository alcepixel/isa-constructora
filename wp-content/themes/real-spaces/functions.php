<?php
/* -----------------------------------------------------------------------------------
  Here we have the custom functions for the theme
  Please be extremely cautious editing this file,
  When things go wrong, they tend to go wrong in a big way.
  You have been warned!
  ----------------------------------------------------------------------------------- */
/*
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link http://codex.wordpress.org/Theme_Development
 * @link http://codex.wordpress.org/Child_Themes
  ----------------------------------------------------------------------------------- */
define('IMIC_THEME_PATH', get_template_directory_uri());
define('IMIC_FILEPATH', get_template_directory());
/* ------------------------------------------------------------------------------------
Add Image Size */
add_image_size('600-400-size',600,400,true);
add_image_size('80-80-size',80,80,true);
add_image_size('150-100-size',150,100,true);
add_image_size('140-47-size',140,47,true);
add_image_size('1200-500-size',1200,500,true);
add_image_size('365-365-size',365,365,true);
add_image_size('100-67-size',100,67,true);
/* -------------------------------------------------------------------------------------
  Load Translation Text Domain
  ----------------------------------------------------------------------------------- */
add_action('after_setup_theme', 'imic_theme_setup');
function imic_theme_setup() {
    load_theme_textdomain('framework', IMIC_FILEPATH . '/language');
}
/* -------------------------------------------------------------------------------------
  Menu option
  ----------------------------------------------------------------------------------- */
function register_menu() {
    register_nav_menu('primary-menu', __('Primary Menu', 'framework'));
}
add_action('init', 'register_menu');
/* -------------------------------------------------------------------------------------
  Set Max Content Width (use in conjuction with ".entry-content img" css)
  ----------------------------------------------------------------------------------- */
if (!isset($content_width))
    $content_width = 680;
/* -------------------------------------------------------------------------------------
  Configure WP2.9+ Thumbnails
  ----------------------------------------------------------------------------------- */
if (function_exists('add_theme_support')) {
    add_theme_support('post-thumbnails');
    add_theme_support('automatic-feed-links');
    set_post_thumbnail_size(958, 9999);
    add_theme_support('post-formats', array(
        'quote', 'image', 'gallery', 'link',
    ));
    
}
/* -------------------------------------------------------------------------------------
  Custom Gravatar Support
  ----------------------------------------------------------------------------------- */
if(!function_exists('imic_custom_gravatar')){
function imic_custom_gravatar($avatar_defaults) {
    $imic_avatar = get_template_directory_uri() . '/images/img_avatar.png';
    $avatar_defaults[$imic_avatar] = 'Custom Gravatar (/images/img_avatar.png)';
    return $avatar_defaults;
}
add_filter('avatar_defaults', 'imic_custom_gravatar');
}
/* -------------------------------------------------------------------------------------
  Load Theme Options
  ----------------------------------------------------------------------------------- */
require_once(IMIC_FILEPATH . '/includes/ReduxCore/framework.php');
require_once(IMIC_FILEPATH . '/includes/sample/sample-config.php');
include_once(IMIC_FILEPATH . '/imic-framework/imic-framework.php');
/* -------------------------------------------------------------------------------------
  For Paginate
  ----------------------------------------------------------------------------------- */
if(!function_exists('pagination')){
function pagination($pages = '', $range = 4) {
        $showitems = ($range * 2) + 1;
	global $paged;
	if (empty($paged))
		$paged = 1;
       if ($pages == '') {
		global $wp_query;
		$pages = $wp_query->max_num_pages;
		if (!$pages) {
			$pages = 1;
		}
	}
        if (1 != $pages) {
		echo '<ul class="pagination">';
		echo '<li><a href="' . get_pagenum_link(1) .'" title="First"><i class="fa fa-chevron-left"></i></a></li>';
                for ($i = 1; $i <= $pages; $i++) {
			if (1 != $pages && (!($i >= $paged + $range + 3 || $i <= $paged - $range - 3) || $pages <= $showitems )) 			{
				echo ($paged == $i) ? "<li class=\"active\"><span>" . $i . "</span></li>" : "<li><a href='" . get_pagenum_link($i) . "' class=\"\">" . $i . "</a></li>";
			}
		}
		echo '<li><a href="'. get_pagenum_link($pages) .'" title="Last"><i class="fa fa-chevron-right"></i></a></li>';
		echo '</ul>';
	}
}
}
/* -------------------------------------------------------------------------------------
  For Remove Dimensions from thumbnail image
  ----------------------------------------------------------------------------------- */
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10);
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10);
function remove_thumbnail_dimensions($html) {
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}
/* -------------------------------------------------------------------------------------
  Excerpt More and  length
  ----------------------------------------------------------------------------------- */
if(!function_exists('imic_custom_read_more')){
function imic_custom_read_more() {
    return '';
}}
if(!function_exists('imic_excerpt')){
function imic_excerpt($limit = '') {
    return '<p>' . wp_trim_words(get_the_excerpt(), $limit, imic_custom_read_more()) . '</p>';
}}
/* ----------------------------------------------------------------------------------- */
/* 	Comment Styling
  /*----------------------------------------------------------------------------------- */
if(!function_exists('imic_comment')){
function imic_comment($comment, $args, $depth) {
    $isByAuthor = false;
    if ($comment->comment_author_email == get_the_author_meta('email')) {
        $isByAuthor = true;
    }
    $GLOBALS['comment'] = $comment;
    ?>
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
        <div class="post-comment-block">
            <div id="comment-<?php comment_ID(); ?>">
                <div class="img-thumbnail"><?php echo get_avatar($comment, $size = '40'); ?></div>
                <?php
                echo preg_replace('/comment-reply-link/', 'comment-reply-link btn btn-primary btn-xs pull-right', get_comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth'], 'reply_text' => 'Reply'))), 1);
                echo '<h5>' . get_comment_author() . ' says</h5>';
                ?>            
                <span class="meta-data">
                    <?php
                    echo date('M d,Y', strtotime(get_comment_date()));
                    _e(' at ', 'framework');
                    echo date('g:i a', strtotime(get_comment_time()));
                    ?>
                </span>
                <?php if ($comment->comment_approved == '0') : ?>
                    <em class="moderation"><?php _e('Your comment is awaiting moderation.','framework') ?></em>
                    <br />
                <?php endif; ?>
                <?php comment_text() ?>
            </div><?php next_comments_link( 'Newer Comments »', 0 ); ?>
	   </div>
            <?php
        }
}

function custom_pagination($numpages = '', $pagerange = '', $paged='') {
 
  if (empty($pagerange)) {
    $pagerange = 2;
  }
 
  /**
   * This first part of our function is a fallback
   * for custom pagination inside a regular loop that
   * uses the global $paged and global $wp_query variables.
   * 
   * It's good because we can now override default pagination
   * in our theme, and use this function in default quries
   * and custom queries.
   */
  global $paged;
  if (empty($paged)) {
    $paged = 1;
  }
  if ($numpages == '') {
    global $wp_query;
    $numpages = $wp_query->max_num_pages;
    if(!$numpages) {
        $numpages = 1;
    }
  }
 
  /** 
   * We construct the pagination arguments to enter into our paginate_links
   * function. 
   */
  $pagination_args = array(
    'base'            => get_pagenum_link(1) . '%_%',
    'format'          => 'page/%#%',
    'total'           => $numpages,
    'current'         => $paged,
    'show_all'        => False,
    'end_size'        => 1,
    'mid_size'        => $pagerange,
    'prev_next'       => True,
    'prev_text'       => __('&laquo;'),
    'next_text'       => __('&raquo;'),
    'type'            => 'plain',
    'add_args'        => false,
    'add_fragment'    => ''
  );

  $paginate_links = paginate_links($pagination_args);
  
  if ($paginate_links) {
    echo "<nav class='custom-pagination'>";
      echo "<span class='page-numbers page-num'>Página " . $paged . " de " . $numpages . "</span> ";
      echo $paginate_links;
    echo "</nav>";
  }
 
}




/*
  Backup/Restore Theme Options
  @ https://digwp.com/2014/04/backup-restore-theme-options/
  Go to "Appearance > Backup Options" to export/import theme settings
  (based on "Gantry Export and Import Options" by Hassan Derakhshandeh)

  Usage:
  1. Add entire backup/restore snippet to functions.php
  2. Edit 'shapeSpace_options' to match your theme options
*/
class backup_restore_theme_options {

  function backup_restore_theme_options() {
    add_action('admin_menu', array(&$this, 'admin_menu'));
  }
  function admin_menu() {
    // add_submenu_page($parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function);
    // $page = add_submenu_page('themes.php', 'Backup Options', 'Backup Options', 'manage_options', 'backup-options', array(&$this, 'options_page'));

    // add_theme_page($page_title, $menu_title, $capability, $menu_slug, $function);
    $page = add_theme_page('Backup Options', 'Backup Options', 'manage_options', 'backup-options', array(&$this, 'options_page'));

    add_action("load-{$page}", array(&$this, 'import_export'));
  }
  function import_export() {
    if (isset($_GET['action']) && ($_GET['action'] == 'download')) {
      header("Cache-Control: public, must-revalidate");
      header("Pragma: hack");
      header("Content-Type: text/plain");
      header('Content-Disposition: attachment; filename="theme-options-'.date("dMy").'.dat"');
      echo serialize($this->_get_options());
      die();
    }
    if (isset($_POST['upload']) && check_admin_referer('shapeSpace_restoreOptions', 'shapeSpace_restoreOptions')) {
      if ($_FILES["file"]["error"] > 0) {
        // error
      } else {
        $options = unserialize(file_get_contents($_FILES["file"]["tmp_name"]));
        if ($options) {
          foreach ($options as $option) {
            update_option($option->option_name, unserialize($option->option_value));
          }
        }
      }
      wp_redirect(admin_url('themes.php?page=backup-options'));
      exit;
    }
  }
  function options_page() { ?>

    <div class="wrap">
      <?php screen_icon(); ?>
      <h2>Backup/Restore Theme Options</h2>
      <form action="" method="POST" enctype="multipart/form-data">
        <style>#backup-options td { display: block; margin-bottom: 20px; }</style>
        <table id="backup-options">
          <tr>
            <td>
              <h3>Backup/Export</h3>
              <p>Here are the stored settings for the current theme:</p>
              <p><textarea class="widefat code" rows="20" cols="100" onclick="this.select()"><?php echo serialize($this->_get_options()); ?></textarea></p>
              <p><a href="?page=backup-options&action=download" class="button-secondary">Download as file</a></p>
            </td>
            <td>
              <h3>Restore/Import</h3>
              <p><label class="description" for="upload">Restore a previous backup</label></p>
              <p><input type="file" name="file" /> <input type="submit" name="upload" id="upload" class="button-primary" value="Upload file" /></p>
              <?php if (function_exists('wp_nonce_field')) wp_nonce_field('shapeSpace_restoreOptions', 'shapeSpace_restoreOptions'); ?>
            </td>
          </tr>
        </table>
      </form>
    </div>

  <?php }
  function _display_options() {
    $options = unserialize($this->_get_options());
  }
  function _get_options() {
    global $wpdb;
    return $wpdb->get_results("SELECT option_name, option_value FROM {$wpdb->options} WHERE option_name = 'real-spaces'"); // edit 'shapeSpace_options' to match theme options
  }
}
new backup_restore_theme_options();
?>