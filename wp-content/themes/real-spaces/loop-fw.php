<?php global $author_id; ?>
<div class="widget hide">
	<h3 class="widgettitle"><?php _e('Agent','framework'); ?></h3>
	<div class="agent">
		<?php $agent_image = get_the_author_meta('agent-image', $author_id);
		$userImage = wp_get_attachment_image_src($agent_image, '600-400-size'); 
		$userFirstName = get_the_author_meta('first_name', $author_id);
		$userLastName = get_the_author_meta('last_name', $author_id);
		$userName = get_userdata( $author_id );
		if(!empty($userFirstName) || !empty($userLastName)) 
		{
			$userName = $userFirstName .' '. $userLastName; 
		} 
		else 
		{ 
			$userName = $userName->user_login; 
		} 
		$description = get_the_author_meta( 'description', $author_id ); 
		if($userImage!='') 
		{ ?>
			<img src="<?php echo $userImage[0]; ?>" alt="" class="margin-20"><?php 
		} ?>
		<h4><a href="<?php echo get_author_posts_url($author_id); ?>"><?php echo $userName; ?></a></h4>
		<?php echo apply_filters('the_content', $description); ?>
		<div class="agent-contacts clearfix">
			<a href="<?php echo get_author_posts_url($author_id); ?>" class="btn btn-primary pull-right btn-sm"><?php _e('Agent Information','framework'); ?></a><a id="show_login" data-target="#agentmodal" data-toggle="modal" class="btn btn-primary pull-right btn-sm"><?php _e('Contact Agent','framework'); ?></a>
			<?php $userFB = get_the_author_meta('fb-link', $author_id);
			$userTWT = get_the_author_meta('twt-link', $author_id);
			$userGP = get_the_author_meta('gp-link', $author_id);
			$userMSG = get_the_author_meta('msg-link', $author_id);
			$userSocialArray = array_filter(array($userFB,$userTWT,$userGP,$userMSG));
			$userSocialClass = array('fa-facebook','fa-twitter','fa-google-plus','fa-envelope');
			if(!empty($userSocialArray)) 
			{
				echo '<ul>';
					foreach($userSocialArray as $key => $value)	
					{
						if(!empty($value)) 
						{
							echo '<li><a href="'. $value .'" target="_blank"><i class="fa '. $userSocialClass[$key] .'"></i></a></li>';
						}
					}
				echo '</ul>';
			} ?>
		</div>
	</div>
</div>
<div class="widget">
<!-- 	<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	<div class="fb-like" data-href="<?php echo get_the_permalink(); ?>" data-layout="button" data-action="like" data-show-faces="true" data-share="true"></div> -->
</div>
<div class="widget">
	<h3 class="widgettitle"><?php _e('Descripción','framework'); ?></h3>
	<div id="description">
		<?php the_content(); ?>
	</div>
</div>

<!-- <div class="widget">
	<h3 class="widgettitle"><?php _e('Comodidades','framework'); ?></h3>
	<div id="amenities">
		<div class="additional-amenities">
			<ul>
			<?php 
			$amenity_array=array();
			$property_amenities = get_post_meta(get_the_ID(),'imic_property_amenities',true);

			//print_r($property_amenities);
			global $imic_options;		
			foreach($property_amenities as $properties_amenities_temp)
			{
				if($properties_amenities_temp!='Not Selected')
				{
					array_push($amenity_array,$properties_amenities_temp);
				}
			}
			// if(isset($imic_options['properties_amenities'])&&count($imic_options['properties_amenities'])>1)
			// {
			// 	foreach($imic_options['properties_amenities'] as $properties_amenities)
			// 	{
	 	// 			$am_name= strtolower(str_replace(' ','',$properties_amenities));
			// 		if(in_array($properties_amenities, $amenity_array))
			// 		{
			// 			$class = 'available';
			// 		}
			// 		else
			// 		{
			// 			$class = 'navailable'; 
	  // 				}
			// 		if(!empty($properties_amenities))
			// 		{
			// 			echo '<li class="'.$class.'"><i class="fa fa-check-square"></i> '.$properties_amenities.'</li>';
			// 		}
			// 	}
			// }

			if(isset($property_amenities))
			{
				foreach($property_amenities as $properties_amenities)
				{
	
						echo '<li class="'.$class.'"><i class="fa fa-check-square"></i> '.$properties_amenities.'</li>';
				}
			}
			$author_id = $post->post_author; ?>
			</ul>
		</div>
	</div>
</div> -->
<div class="widget hide">
	<h3 class="widgettitle"><?php _e('Detalles del Proyecto','framework'); ?></h3>
	<div id="address" class="tab-pane">
		<?php imicPropertyDetailById(get_the_ID()); ?>
	</div>
</div>
</div>