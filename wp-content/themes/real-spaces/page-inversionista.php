<?php get_header();
/* Site Showcase */
imic_page_banner($pageID = get_the_ID());
/* End Site Showcase */
/* Login/Register Page Design Layout 
============================================*/
$pageLayout = get_post_meta(get_the_ID(),'imic_register_layout',true);
$contentClass = 6;
if ($pageLayout != 1) { $contentClass = 6; }?>
<!-- Start Content -->
<div class="main" role="main">
  <div id="content" class="content full">
    <div class="container">
      <div class="page">
        <div class="row">
          <?php echo '<div class="col-md-'.$contentClass.' col-sm-'.$contentClass.'">';
            /* Page Content =====================*/
            while (have_posts()):the_post();
              the_content();
            endwhile;
          echo '</div>';
          /* Manage Login Form ========================*/
          if ($pageLayout == 1 || $pageLayout == 2) { ?>
            <div class="col-md-6 col-sm-6 login-form">
              <h3><?php _e('Accesos','framework'); ?></h3>
              <form id="login" action="login" method="post">
                <?php 
                  $redirect_login= get_post_meta(get_the_ID(),'imic_login_redirect_options',true);
                  $redirect_login=!empty($redirect_login)?$redirect_login:  home_url();
                ?>
                <input type ="hidden" class ="redirect_login" name ="redirect_login" value ="<?php echo $redirect_login ?>"/>
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-user"></i></span>
                  <input class="form-control input1" id="loginname" type="text" name="loginname">
                </div>
                <br>
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                  <input class="form-control input1" id="password" type="password" name="password">
                </div>
                <div class="checkbox">
                  <input type="checkbox" checked="checked" value="true" name="rememberme" id="rememberme" class="checkbox"> <?php _e('Recordar','framework'); ?>
                </div>
                <input class="submit_button btn btn-primary button2" type="submit" value="<?php _e('Ingresar','framework'); ?>" name="submit">
                <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?><p class="status"></p>
              </form>
            </div>
          
          <?php } ?>

        </div>
      </div>
    </div>
  </div>
</div>
<?php  get_footer(); ?>