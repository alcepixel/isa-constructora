<?php get_header();
/* Site Showcase */
imic_page_banner($pageID = get_the_ID());
/* End Site Showcase */
/* Login/Register Page Design Layout 
============================================*/
$pageLayout = get_post_meta(get_the_ID(),'imic_register_layout',true);
$contentClass = 4;
if ($pageLayout != 1) { $contentClass = 6; }?>
<!-- Start Content -->
<div class="main" role="main">
  <div id="content" class="content full">
    <div class="container">
      <div class="page">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <?php while (have_posts()):the_post(); the_content(); endwhile; ?>
          </div>          
        </div>
      </div>
    </div>
  </div>
</div>
<?php  get_footer(); ?>