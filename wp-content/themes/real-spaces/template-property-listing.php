<?php
/*
Template Name: Property Listing
*/
get_header(); 
/* Site Showcase */
imic_page_banner($pageID = get_the_ID());
/* End Site Showcase */
$sidebar = get_post_meta(get_the_ID(),'imic_select_sidebar_from_list',false); 
$design_type = get_post_meta(get_the_ID(),'imic_property_design_layout',true);
$prop_type = get_post_meta(get_the_ID(),'imic_property_type_list',true);
$class = (empty($sidebar)||!is_active_sidebar($sidebar[0]))?12:9;
$div = ($design_type=='grid')?'<div class="property-grid"><ul id="property_grid_holder" class="grid-holder col-4">':'<div class="property-listing row"><ul class="col-md-12" id="property_grid_holder">'; 
$layout = ($design_type=='grid')?'<i class="fa fa-th-large"></i></span>'.__('Property Grid','framework').'':'<i class="fa fa-th-list"></i></span>'.__('Property Listing','framework'); 
$listing_layout_url = get_post_meta(get_the_ID(),'imic_property_listing_url',true); 
$grid_layout_url = get_post_meta(get_the_ID(),'imic_property_grid_url',true); 
$listing_url = ($design_type=='listing')?$listing_layout_url:''; 
$grid_url = ($design_type=='grid')?$grid_layout_url:''; 
$listing_class = ($design_type=='listing')?'active':''; 
$grid_class = ($design_type=='grid')?'active':''; ?>
<!-- Start Content -->
  <div class="main" role="main">
      <div id="content" class="content full">
          <div class="container">
              <div class="row">
           <div class="col-md-<?php echo $class; ?>">
              <div class="property-grid">
                      <ul class="grid-holder col-4">

                      <?php 
                            query_posts(array('post_type'=>'property','post_status'=>'publish','property-type'=>$prop_type,'posts_per_page' => -1));
                if(have_posts()):while(have_posts()):the_post(); 

                    
                    $property_id = get_post_meta(get_the_ID(),'imic_property_site_id',true);
                    $property_images = get_post_meta(get_the_ID(),'imic_property_sights',false);
                $total_images = count($property_images);
                    $property_area = get_post_meta(get_the_ID(),'imic_property_area',true); 
                  $property_baths = get_post_meta(get_the_ID(),'imic_property_baths',true);
                  $property_beds = get_post_meta(get_the_ID(),'imic_property_beds',true);
                  $property_parking = get_post_meta(get_the_ID(),'imic_property_parking',true); 
                  $property_address = get_post_meta(get_the_ID(),'imic_property_site_address',true);
                  $property_city = get_post_meta(get_the_ID(),'imic_property_site_city',true);
                  $property_price = get_post_meta(get_the_ID(),'imic_property_price',true); 
                  $contract = wp_get_object_terms( get_the_ID(), 'property-contract-type', array('fields'=>'ids')); 
                  if(!empty($contract)) {
                  $term = get_term( $contract[0], 'property-contract-type' ); $contract_name = $term->name; } ?>
                        <li class="grid-item type-<?php echo $contract_name; ?>">
                          <div class="property-block"><?php if ( '' != get_the_post_thumbnail() ) { ?><a href="<?php the_permalink(); ?>" class="property-featured-image"> <?php the_post_thumbnail('250-167-size'); ?> <span class="images-count"><i class="fa fa-picture-o"></i> <?php echo $total_images; ?></span> <span class="badges hide"><?php echo $contract_name; ?></span> </a><?php } ?>
                            <div class="property-info">
                              <h4><a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a><span class="pid hide"> (<?php echo $property_id; ?>)</span></h4>
                              <span class="location"><?php echo $property_city; ?></span>
                              <div class="price hide"><strong><?php echo $currency_symbol; ?></strong><span><?php echo $property_price; ?></span></div>
                            </div>
                            <div class="property-amenities clearfix hide"> <span class="area"><strong><?php echo $property_area; ?></strong><?php _e('Area','framework'); ?></span> <span class="baths"><strong><?php echo $property_baths; ?></strong><?php _e('Baths','framework'); ?></span> <span class="beds"><strong><?php echo $property_beds; ?></strong><?php _e('Beds','framework'); ?></span> <span class="parking"><strong><?php echo $property_parking; ?></strong><?php _e('Parking','framework'); ?></span> </div>
                          </div>
                        </li>
                        <?php endwhile; endif; ?>
                      </ul>
                    </div>
                       </div>
                      <?php pagination(); wp_reset_query(); ?>
                  </div>
             
              </div>
          </div>
      </div>
  <?php get_footer(); ?>