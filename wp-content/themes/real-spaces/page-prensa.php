<?php get_header();
/* Site Showcase */
imic_page_banner($pageID = get_the_ID());
/* End Site Showcase */
/* Login/Register Page Design Layout 
============================================*/
$pageLayout = get_post_meta(get_the_ID(),'imic_register_layout',true);
$contentClass = 4;
if ($pageLayout != 1) { $contentClass = 6; }  
?>
    <div class="ultimos_post">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-xs-12">            
              <div class="row">
                <?php
                $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                $args = array(
                 'post_type'       => 'post',
                  'posts_per_page'  => 12,
                  'orderby'         => 'id',
                  'order'           => 'desc',
                  'paged'           => $paged
                );
                $latest = new WP_Query($args);
                while($latest->have_posts()): $latest->the_post(); ?>
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="block">
                      <?php the_post_thumbnail('365-365-size',array('class' => 'img-responsive')); ?>
                      <div class="titlee">
                        <h4 style="height: 70px;"><?php the_title();?> </h4> 
                        <div class="meta pull-left"><?php echo get_the_date(); ?></div>                     
                        <a href="<?php the_permalink(); ?>"  class="btn_ver_mas pull-right">Ver Más</a>  
                      </div>                      
                    </div>
                  </div>              
                <?php endwhile;?> 
                <?php wp_reset_query();?>         
              </div>            
          </div>
        </div>
        <div class="row">
          
          <?php 
                        if (function_exists(custom_pagination)) { 
                            custom_pagination($latest->max_num_pages,"",$paged); 
                        } 
                    ?>
        </div>
      </div>
    </div>
<?php  get_footer(); ?>