<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="es"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        
        <title>ISA - Empresa Constructora</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css?002">
        
		<!--Libreria Modernizr-->
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
		<script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        
          ga('create', 'UA-43025102-1', 'isa.cl');
          ga('send', 'pageview');        
        </script>        
        
        <!--Iconos del escritorio del Navegador-->
        <link rel="shortcut icon" href="ico/favicon.ico">
    	<link rel="apple-touch-icon-precomposed" href="icons/apple-touch-icon-precomposed.png">
    	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="icons/apple-touch-icon-114x114-precomposed.png">
    	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="icons/apple-touch-icon-72x72-precomposed.png">
    	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="icons/apple-touch-icon-57x57-precomposed.png">         
    </head>

    <!--Comienzo del body--> 
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
        <div id="wrap">
        
        	<header>
            	<div id="logo">
                	<a href="index.html"><img src="img/logo_constructora.jpg" width="182" height="73" alt="Logo Constructora ISA"></a>
                </div>
                
                <div id="un_paso_adelante">
                	   <img src="img/un_paso_adelante.jpg" width="151" height="50" alt="Un paso adelante">             
                </div>                 
                
                <div id="logo_iso9001">
                	<img src="img/logo_iso9001_b.jpg" width="346" height="104" alt="Logo_Iso_9001">
                </div>
                
                <div style="clear:both"></div> 
                
                <nav>
                	<a href="empresa.html">EMPRESA</a>
                    <a href="calidad.html">CALIDAD</a>
                    <a href="obras_ejecucion.html">OBRAS</a>
                    <a href="post_venta.html">POST VENTA</a>
                    <a href="prensa.html">PRENSA</a>
                    <a href="proveedores.php">PROVEEDORES</a>                     
                    <a href="contacto.php" class="boton_marcado">CONTACTO</a>
                </nav>                                        
            
            <div id="contenido">  
            
            <div id="contenedor_formulario">                
                 
              <?php
                if(isset($_POST['boton'])){
                    $errors = array(); // declaramos un array para almacenar los errores
                    if($_POST['nombre'] == ''){
                        $errors[1] = '<span class="error">Ingrese su nombre</span>';
					}else if($_POST['email'] == '' or !preg_match("/^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/",$_POST['email'])){
                        $errors[3] = '<span class="error">Ingrese un email correcto</span>';
                    }else{
						$dest = "info@isa.cl";
						$nombre = $_POST['nombre'];
						$email = $_POST['email'];
						$telefono = $_POST['telefono'];
						$empresa = $_POST['empresa'];

						$cuerpo = "Nombre: ".$_POST['nombre']."<br>";
						$cuerpo .= "Email: ".$_POST['email']."<br>";					
						$cuerpo .= "Teléfono: ".$_POST['telefono']."<br>";
						$cuerpo .= "Empresa: ".$_POST['empresa']."<br>";
						$cuerpo .= "Mensaje: ".$_POST['mensaje']."<br>";
												
												
						
						$headers = "From: $nombre $email\r\n"; //Quien envia?
						$headers .= "X-Mailer: PHP5\n";
						$headers .= 'MIME-Version: 1.0' . "\n";
						$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";										
						
								if(mail($dest,$nombre,$cuerpo,$headers)){ // esta es una comparacion para ver si envio el mail o no
								$result = '<div class="result_ok">Email enviado correctamente</div>';
								// si el envio fue exitoso reseteamos lo que el usuario escribio:
								$_POST['nombre'] = '';
								$_POST['email'] = '';
								$_POST['telefono'] = '';
								$_POST['empresa'] = '';
								$_POST['mensaje'] = '';
							}else{
								$result = '<div class="result_fail">Hubo un error al enviar el mensaje</div>';
							}							
                    }
                }
            ?>                   
              
                <form class='contacto' method="post" action="">
                    <div><label>Nombre:</label><input name="nombre" type='text' class="nombre" value='<?php echo $_POST['nombre']; ?>'></div>
                    <?php echo $errors[1]; ?>

                    <div><label>Email:</label><input name="email" type='text' class="email" value='<?php echo $_POST['email']; ?>'></div>
                    <?php echo $errors[3]; ?>
                    <div><label>Teléfono:</label><input name="telefono" type='text' class="telefono" value='<?php echo $_POST['telefono']; ?>'></div>              
                     <?php echo $errors[4]; ?>
                    <div><label>Empresa:</label><input name="empresa" type='text' class="empresa" value='<?php echo $_POST['empresa']; ?>'></div>                 
					<?php echo $errors[5]; ?>
                    <div><label>Mensaje:</label><textarea rows='6' class="mensaje" name="mensaje"></textarea></div>
                    <?php echo $errors[6]; ?>
                    <div><input type='submit' value='Enviar' class="boton" name="boton"> <?php echo $_POST['mensaje']; ?></div>
                    
                <?php echo $result; ?>    
                </form>    
                
                </div> <!--Cierre Contenedor Formulario-->              
                                                                                           
            </div> <!--Cierre contenido-->
            
            <div style="clear:both"></div>                         
            <div id="linea_sola"></div>             
         
			<footer>
            	<div id="logo_footer">
                	<img src="img/logo_constructora_footer.jpg" width="145" height="58" alt="Logo Footer">
                </div>            	
                
                <div class="inverisa_logo">
                    <a href="http://www.inverisa.cl" target="_blank"><img src="img/inverisa_logo.jpg"></a>
                </div>            	
                <div id="contenido_footer"><p>Av. Kennedy 7600 piso 6, Vitacura - Fono: 22 5136 800 / 22 8649 308 - Diseñado por <a href="www.almarzadiseno.cl" target="_blank">Almarza</a></p></div>            
            </footer>
            
        </div> <!--Cierre del Wrap-->
        
        
        

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <script>
            var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src='//www.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
    </body>
    <!--Cierre del body--> 
</html>
